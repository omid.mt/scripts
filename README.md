# scripts

Common scripts for linux, mac os and even windows machines to automate or simplify things.

## Shell Prompt
### Install
Get a local copy of repository somewhere like your home directory, then source it from /etc/profile or ~/.bashrc

```bash
cd ~ ; git clone https://gitlab.com/omid.mt/scripts.git

# or cd ~ ; curl -o scripts.tar.gz https://gitlab.com/omid.mt/scripts/-/archive/master/scripts-master.tar.gz && tar xvf scripts.tar.gz && mv scripts-master/ scripts/

# Source it in .bashrc
 . ~/scripts/.prompt.sh 
 ```
In case of /etc/prifile to apply it globally for all users, use absolute directory path and ensure the direcotry and files are readable for all users.