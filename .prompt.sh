# Custom shell prompt includes git status
# Author: omid.mt@gmail.com
#

shopt -s histappend

export PROMPT_COMMAND=__prompt_command

export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1

# https://stackoverflow.com/questions/59895/
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

. $DIR/git-prompt.sh

function __prompt_command() {
    local EXIT_STATUS="$?"
    history -a

    ### Thanks to demure https://notabug.org/demure/dotfiles/src/master/subbash/prompt
    ### Colors to Vars ### {{{
    ## http://wiki.archlinux.org/index.php/Color_Bash_Prompt#List_of_colors_for_prompt_and_Bash
    ## Terminal Control Escape Sequences: http://www.termsys.demon.co.uk/vtansi.htm
    ## https://gist.github.com/bcap/5682077#file-terminal-control-sh
    ## Can unset with `unset -v {,B,U,I,BI,On_,On_I}{Bla,Red,Gre,Yel,Blu,Pur,Cya,Whi} RCol`
    local ResetColor='\[\e[0m\]'  # Text Reset

    # Regular
    local Bla='\[\e[0;30m\]'
    local Red='\[\e[0;31m\]'
    local Gre='\[\e[0;32m\]'
    local Yel='\[\e[0;33m\]'
    local Blu='\[\e[0;34m\]'
    local Pur='\[\e[0;35m\]'
    local Cya='\[\e[0;36m\]'
    local Whi='\[\e[0;37m\]'

    # Bold  
    local BBla='\[\e[1;30m\]'
    local BRed='\[\e[1;31m\]'
    local BGre='\[\e[1;32m\]'
    local BYel='\[\e[1;33m\]'
    local BBlu='\[\e[1;34m\]'
    local BPur='\[\e[1;35m\]'
    local BCya='\[\e[1;36m\]'
    local BWhi='\[\e[1;37m\]'

    # Underline
    local UBla='\[\e[4;30m\]'
    local URed='\[\e[4;31m\]'
    local UGre='\[\e[4;32m\]'
    local UYel='\[\e[4;33m\]'
    local UBlu='\[\e[4;34m\]'
    local UPur='\[\e[4;35m\]'
    local UCya='\[\e[4;36m\]'
    local UWhi='\[\e[4;37m\]'

    # High Intensity
    local IBla='\[\e[0;90m\]'
    local IRed='\[\e[0;91m\]'
    local IGre='\[\e[0;92m\]'
    local IYel='\[\e[0;93m\]'
    local IBlu='\[\e[0;94m\]'
    local IPur='\[\e[0;95m\]'
    local ICya='\[\e[0;96m\]'
    local IWhi='\[\e[0;97m\]'

    # BoldHigh Intensity
    local BIBla='\[\e[1;90m\]'
    local BIRed='\[\e[1;91m\]'
    local BIGre='\[\e[1;92m\]'
    local BIYel='\[\e[1;93m\]'
    local BIBlu='\[\e[1;94m\]'
    local BIPur='\[\e[1;95m\]'
    local BICya='\[\e[1;96m\]'
    local BIWhi='\[\e[1;97m\]'

    # Background
    local On_Bla='\e[40m'
    local On_Red='\e[41m'
    local On_Gre='\e[42m'
    local On_Yel='\e[43m'
    local On_Blu='\e[44m'
    local On_Pur='\e[45m'
    local On_Cya='\e[46m'
    local On_Whi='\e[47m'

    # High Intensity Backgrounds
    local On_IBla='\[\e[0;100m\]'
    local On_IRed='\[\e[0;101m\]'
    local On_IGre='\[\e[0;102m\]'
    local On_IYel='\[\e[0;103m\]'
    local On_IBlu='\[\e[0;104m\]'
    local On_IPur='\[\e[0;105m\]'
    local On_ICya='\[\e[0;106m\]'
    local On_IWhi='\[\e[0;107m\]'
    ### End Color Vars ### }}}

    if ! hash __git_ps1 2>/dev/null; then
    __git_ps1 () { echo -n -e '!'; }
    fi

    local SMILEY="${BGre}:)"
    if [ ${EXIT_STATUS} != 0 ]; then
        local SMILEY="${BRed}${EXIT_STATUS} :("
    fi

    local SESSION_TYPE="local"
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        SESSION_TYPE="remote"
    # Docker console
    elif [[ $PPID -eq 0 ]]; then
        SESSION_TYPE="local"
    else
        case $(ps -o comm= -p $PPID) in
            sshd|*/sshd) SESSION_TYPE="remote";;
        esac
    fi

    if [[ "$SHELL" = *bash* ]] && ( [[ "$TERM" = *xterm* ]] || [[ "$TERM" = *vt100* ]] ); then
        USER_ID=`id | cut -d"(" -f2 | cut -f1 -d ")"`
        if [ "$USER_ID" = "root" ]; then
            local Prompt_Sign="#"
            local TimeColor="${BRed}"
            local UserColor="${BRed}"
            local DirectoryColor="${BBlu}"
            local HostColor="${BRed}"
            local GitColor="${IPur}"
        else
            local Prompt_Sign="$"
            local TimeColor="${BGre}"
            local UserColor="${BGre}"
            local DirectoryColor="${BBlu}"
            local HostColor="${IWhi}"
            local GitColor="${IPur}"
        fi

        if [[ "$SESSION_TYPE" = "remote" ]]; then
            HostColor="${IYel}"
        fi

        PS1="${TimeColor}[\t] ${User_Color}\u@${HostColor}\h ${DirectoryColor}\W ${GitColor}\`__git_ps1 "[%s]"\`${SMILEY}\n\\$ ${ResetColor}"
    fi
}

alias ll='ls -lthra'
alias g='git'
alias d=docker
alias k=kubectl

if [[ ${MACHTYPE} =~ x86_64-apple-darwin ]]; then
    if [ -f "$(xcode-select --print-path)/usr/share/git-core/git-completion.bash" ] ; then
        source "$(xcode-select --print-path)/usr/share/git-core/git-completion.bash"
        source "$(xcode-select --print-path)/usr/share/git-core/git-prompt.sh"
    fi
    alias screenlock='open /System/Library/CoreServices/ScreenSaverEngine.app'
fi

HISTSIZE=10000
HISTFILESIZE=20000
